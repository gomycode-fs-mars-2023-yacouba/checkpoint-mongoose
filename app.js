const mongoose = require('mongoose');
require('dotenv').config();

// Connectez-vous à la base de données MongoDB
mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true });

// Créez un schéma pour la personne
const personSchema = new mongoose.Schema({
  name: { type: String, required: true },
  age: Number,
  favoriteFoods: [String]
});

// Créez un modèle basé sur le schéma de personne
const Person = mongoose.model('Person', personSchema);

// Créez et enregistrez un enregistrement d'un modèle
const createPerson = (done) => {
  const person = new Person({
    name: 'John Doe',
    age: 25,
    favoriteFoods: ['Pizza', 'Burger']
  });

  person.save((err, data) => {
    if (err) return console.error(err);
    done(null, data);
  });
};

// Créez plusieurs personnes avec model.create()
const createManyPeople = (arrayOfPeople, done) => {
  Person.create(arrayOfPeople, (err, data) => {
    if (err) return console.error(err);
    done(null, data);
  });
};

// Utilisez model.find() pour rechercher des personnes par nom
const findPeopleByName = (personName, done) => {
  Person.find({ name: personName }, (err, data) => {
    if (err) return console.error(err);
    done(null, data);
  });
};

// Utilisez model.findOne() pour rechercher une personne par aliment favori
const findOnePerson = (food, done) => {
  Person.findOne({ favoriteFoods: food }, (err, data) => {
    if (err) return console.error(err);
    done(null, data);
  });
};

// Utilisez model.findById() pour rechercher une personne par _id
const findPersonById = (personId, done) => {
  Person.findById(personId, (err, data) => {
    if (err) return console.error(err);
    done(null, data);
  });
};

// Effectuez une mise à jour et enregistrez la personne mise à jour
const findEditThenSave = (personId, done) => {
  const foodToAdd = 'Hamburger';

  Person.findById(personId, (err, person) => {
    if (err) return console.error(err);

    person.favoriteFoods.push(foodToAdd);
    person.save((err, data) => {
      if (err) return console.error(err);
      done(null, data);
    });
  });
};

// Effectuez une mise à jour avec findOneAndUpdate()
const findAndUpdate = (personName, done) => {
  const ageToUpdate = 20;

  Person.findOneAndUpdate(
    { name: personName },
    { age: ageToUpdate },
    { new: true },
    (err, data) => {
      if (err) return console.error(err);
      done(null, data);
    }
  );
};

// Supprimez une personne par _id
const removeById = (personId, done) => {
  Person.findByIdAndRemove(personId, (err, data) => {
    if (err) return console.error(err);
    done(null, data);
  });
};

// Supprimez toutes les personnes dont le nom est "Mary"
const removeManyPeople = (done) => {
  const personName = 'Mary';

  Person.remove({ name: personName }, (err, data) => {
    if (err) return console.error(err);
    done(null, data);
  });
};

// Chaînez les assistants de requête de recherche pour affiner les résultats
const queryChain = (done) => {
  const foodToSearch = 'Burritos';

  Person.find({ favoriteFoods: foodToSearch })
    .sort('name')
    .limit(2)
    .select('-age')
    .exec((err, data) => {
      if (err) return console.error(err);
      done(null, data);
    });
};

// Appel des fonctions une par une avec des rappels appropriés
createPerson((err, data) => {
  if (err) return console.error(err);
  console.log(data);

  const arrayOfPeople = [
    { name: 'Alice', age: 28, favoriteFoods: ['Sushi', 'Pasta'] },
    { name: 'Bob', age: 30, favoriteFoods: ['Burger', 'Steak'] },
    { name: 'Mary', age: 35, favoriteFoods: ['Pizza', 'Ice Cream'] }
  ];

  createManyPeople(arrayOfPeople, (err, data) => {
    if (err) return console.error(err);
    console.log(data);

    findPeopleByName('Alice', (err, data) => {
      if (err) return console.error(err);
      console.log(data);

      findOnePerson('Burger', (err, data) => {
        if (err) return console.error(err);
        console.log(data);

        findPersonById(data._id, (err, data) => {
          if (err) return console.error(err);
          console.log(data);

          findEditThenSave(data._id, (err, data) => {
            if (err) return console.error(err);
            console.log(data);

            findAndUpdate('Alice', (err, data) => {
              if (err) return console.error(err);
              console.log(data);

              removeById(data._id, (err, data) => {
                if (err) return console.error(err);
                console.log(data);

                removeManyPeople((err, data) => {
                  if (err) return console.error(err);
                  console.log(data);

                  queryChain((err, data) => {
                    if (err) return console.error(err);
                    console.log(data);
                  });
                });
              });
            });
          });
        });
      });
    });
  });
});
